import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  Divider,
  List,
  ListItem,
  ListItemText,
  Button,
} from '@material-ui/core';

const drawerStyle = makeStyles(theme => ({
  root: {},
  header: {
    display: 'flex',
    flexDirection: 'row',
    height: 56,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  toolbar: theme.mixins.toolbar,
}));

const Drawer = props => {
  const { Link, HandleDrawerToggle } = props;
  const classes = drawerStyle();

  return (
    <div>
      <div className={classes.toolbar}>
        <div className={classes.header}>
          <Typography>Gemas</Typography>
        </div>
      </div>
      <Divider />
      <List>
        <ListItem
          button
          component={Link}
          to="/"
          onClick={() => HandleDrawerToggle()}
        >
          <ListItemText primary="Home" />
        </ListItem>

        <ListItem
          button
          component={Link}
          to="/login"
          onClick={() => HandleDrawerToggle()}
        >
          <ListItemText primary="Log in" />
        </ListItem>

        <ListItem
          button
          component={Link}
          to="/formulario"
          onClick={() => HandleDrawerToggle()}
        >
          <ListItemText primary="Formulario" />
        </ListItem>
      </List>
    </div>
  );
};

export default Drawer;
