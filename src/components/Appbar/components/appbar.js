import React, { useState, useContext } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {
  AppBar,
  Toolbar,
  Hidden,
  IconButton,
  Button,
  Dialog,
  DialogContent,
} from '@material-ui/core';
import { Menu as MenuIcon } from '@material-ui/icons';

// components
import Login from '../../../Pages/Login/login';

// context
import AuthContext from '../../../contexts/authContext';

const appBarStyle = makeStyles(theme => ({
  root: {},
  logo: {
    flexGrow: 1,
  },
  actions: {
    display: 'flex',
    alignItems: 'center',
  },
  authButton: {
    margin: `${theme.spacing(0)}px ${theme.spacing(2)}px`,
  },
}));

const AppBarComponent = props => {
  const { Link, HandleDrawerToggle } = props;
  const classes = appBarStyle();

  const authContext = useContext(AuthContext);
  const { login, handleLogout } = authContext;

  const [dialogOpen, setDialogOpen] = useState(false);

  const handleOpenDialog = () => {
    setDialogOpen(true);
  };

  const handleCloseDialog = () => {
    setDialogOpen(false);
  };

  return (
    <>
      <AppBar position="fixed" className={classes.appBar} color="default">
        <Toolbar>
          <Hidden smUp implementation="css">
            <IconButton onClick={() => HandleDrawerToggle()}>
              <MenuIcon />
            </IconButton>
          </Hidden>
          <div className={classes.logo}>
            <Button component={Link} to="/">
              Gemas
            </Button>
          </div>

          <Hidden xsDown implementation="css">
            <div className={classes.actions}>
              <Button variant="contained" component={Link} to="/formulario">
                Formulario
              </Button>

              {login ? (
                <Button
                  className={classes.authButton}
                  variant="contained"
                  color="secondary"
                  onClick={handleLogout}
                >
                  Log out
                </Button>
              ) : (
                <Button
                  className={classes.authButton}
                  variant="text"
                  onClick={handleOpenDialog}
                >
                  Log in
                </Button>
              )}
            </div>
          </Hidden>
        </Toolbar>
      </AppBar>
      <Dialog
        fullWidth
        maxWidth="sm"
        open={dialogOpen}
        onClose={handleCloseDialog}
      >
        <DialogContent>
          <Login closeDialog={handleCloseDialog} />
        </DialogContent>
      </Dialog>
    </>
  );
};

export default AppBarComponent;
