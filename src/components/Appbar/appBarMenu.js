import React, { useState } from "react";
import { Link } from "react-router-dom";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { CssBaseline, Hidden, Drawer } from "@material-ui/core";

// components
import DrawerComponent from "./components/drawer";
import AppBarComponent from "./components/appbar";

const drawerWidth = 240;
const styles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(0)
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0
    }
  },
  drawerPaper: {
    width: drawerWidth
  },
  toolbar: theme.mixins.toolbar
}));

const AppBarMenu = props => {
  const { container, children } = props;
  const theme = useTheme();
  const classes = styles();
  const [mobileOpen, setMobileOpen] = useState(false);
  const HandleDrawerToggle = () => setMobileOpen(!mobileOpen);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBarComponent Link={Link} HandleDrawerToggle={HandleDrawerToggle} />
      <nav className={classes.drawer}>
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === "rtl" ? "right" : "left"}
            open={mobileOpen}
            onClose={HandleDrawerToggle}
            classes={{
              paper: classes.drawerPaper
            }}
            ModalProps={{
              keepMounted: true // Better open performance on mobile.
            }}
          >
            <DrawerComponent
              Link={Link}
              HandleDrawerToggle={HandleDrawerToggle}
            />
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {children}
      </main>
    </div>
  );
};

export default AppBarMenu;
