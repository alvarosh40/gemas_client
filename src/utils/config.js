// el backend aqui
const API = {
  PORT: '',
  URL: 'https://gemas-api-skgzm.ondigitalocean.app',
  URL_LOCAL: 'http://127.0.0.1:5000',
  END_POINT: 'api/v1',
};

export default {
  API: `${API.URL}/${API.END_POINT}`,
};
