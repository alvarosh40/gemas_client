import { useQuery, useMutation, queryCache } from 'react-query';
import Axios from 'axios';
import Config from './config';

const formsURL = `${Config.API}/form`;
const productoresURL = `${Config.API}/productores`;
const registerURL = `${Config.API}/registros`;

const GetlistOfForm = async () => {
  try {
    const { data } = await Axios.get(`${formsURL}`);
    return data;
  } catch (error) {
    return [];
  }
};

const getListOfProductores = async () => {
  try {
    const { data } = await Axios.get(`${productoresURL}`);
    return data;
  } catch (error) {
    return [];
  }
};

const getListOfRegisterByProducerID = async (_, producerID) => {
  try {
    const { data } = await Axios.get(`${registerURL}/productor/${producerID}`);
    return data;
  } catch (error) {
    return [];
  }
};

const PostForm = async newForm => {
  try {
    const { data } = await Axios.post(`${formsURL}`, { form: newForm });
    return data;
  } catch (error) {
    console.log(error);
  }
};

const postProductores = async newProductor => {
  try {
    const { data } = await Axios.post(`${productoresURL}`, {
      form: newProductor,
    });
    return data;
  } catch (error) {
    return {};
  }
};

const postRegistros = async newRegistro => {
  try {
    const { data } = await Axios.post(`${registerURL}`, {
      form: newRegistro,
    });

    return data;
  } catch (error) {
    return {};
  }
};

const PutForm = async form => {
  try {
    const { data } = await Axios.put(`${formsURL}/${form.id}`, { form });
    return data;
  } catch (error) {
    console.log(error);
  }
};

const putProductores = async productor => {
  try {
    const { data } = await Axios.put(`${productoresURL}/${productor.id}`, {
      form: productor,
    });
    return data;
  } catch (error) {
    return {};
  }
};

const putRegistros = async registro => {
  try {
    const { data } = await Axios.put(`${registerURL}/${registro.id}`, {
      form: registro,
    });
    return data;
  } catch (error) {
    return {};
  }
};

const DeleteForm = async id => {
  try {
    const { data } = await Axios.delete(`${formsURL}/${id}`);
    return data;
  } catch (error) {
    console.log(error);
  }
};

const deleteProductores = async id => {
  try {
    const { data } = await Axios.delete(`${productoresURL}/${id}`);
    return data;
  } catch (error) {
    return {};
  }
};

const deleteRegistros = async id => {
  try {
    const { data } = await Axios.delete(`${registerURL}/${id}`);
    return data;
  } catch (error) {
    return {};
  }
};

export function useGetListOfProductores() {
  return useQuery('productores', getListOfProductores, {
    refetchOnWindowFocus: false,
  });
}

export function useGetListOfRegisterByProducerID(producerID) {
  return useQuery(['registros', producerID], getListOfRegisterByProducerID);
}

export function useGetListOfForm() {
  return useQuery('forms', GetlistOfForm);
}

export function usePostForm() {
  return useMutation(PostForm, {
    onSuccess: () => {
      queryCache.invalidateQueries('forms');
    },
  });
}

export function usePostProductores() {
  return useMutation(postProductores, {
    onSuccess: () => {
      queryCache.invalidateQueries('productores');
    },
  });
}

export function usePostRegistros() {
  return useMutation(postRegistros, {
    onSuccess: () => {
      queryCache.invalidateQueries('registros');
    },
  });
}

export function usePutForm() {
  return useMutation(PutForm, {
    onSuccess: () => {
      queryCache.invalidateQueries('forms');
    },
  });
}

export function usePutProductores() {
  return useMutation(putProductores, {
    onSuccess: () => {
      queryCache.invalidateQueries('productores');
    },
  });
}

export function usePutRegistros() {
  return useMutation(putRegistros, {
    onSuccess: () => {
      queryCache.invalidateQueries('registros');
    },
  });
}

export function useDeleteForm() {
  return useMutation(DeleteForm, {
    onSuccess: () => {
      queryCache.invalidateQueries('forms');
    },
  });
}

export function useDeleteProductores() {
  return useMutation(deleteProductores, {
    onSuccess: () => {
      queryCache.invalidateQueries('productores');
    },
  });
}

export function useDeleteRegistros() {
  return useMutation(deleteRegistros, {
    onSuccess: () => {
      queryCache.invalidateQueries('registros');
    },
  });
}
