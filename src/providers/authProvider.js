import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import Axios from 'axios';
import Config from '../utils/config';
import AuthContext from '../contexts/authContext';

const authURL = `${Config.API}/auth`;

const AuthProvider = props => {
  const mLocalStorage = localStorage.getItem('store');
  const mPersistData = mLocalStorage ? JSON.parse(mLocalStorage) : {};
  const history = useHistory();

  const [login, setLogin] = useState(
    mLocalStorage ? mPersistData.login : false,
  );

  const handleLogin = async payload => {
    try {
      const { data } = await Axios.post(`${authURL}/login`, {
        username: payload.username,
        password: payload.password,
      });
      if (data.login) {
        setLogin(data.login);
        localStorage.setItem(
          'store',
          JSON.stringify({
            login: data.login,
          }),
        );
        history.push('/');

        return data.login;
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleLogOut = async () => {
    localStorage.clear();
    setLogin(false);
    history.push('/');
  };

  return (
    <AuthContext.Provider
      value={{
        login,
        handleLogin,
        handleLogout: handleLogOut,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
