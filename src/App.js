import React from 'react';

import { Switch, Route } from 'react-router-dom';

// components
import AppbarMenu from './components/Appbar/appBarMenu';

// pages
import FormsPage from './Pages/forms/forms';
import HomePage from './Pages/Home/home';
import FormularioPage from './Pages/formulario/formulario';

function App() {
  return (
    <>
      <AppbarMenu>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/formulario" component={FormularioPage} />
          <Route path="/form" component={FormsPage} />
        </Switch>
      </AppbarMenu>
    </>
  );
}

export default App;
