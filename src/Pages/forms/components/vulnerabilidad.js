import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, Grid } from "@material-ui/core";

import cuestionario from "./../cuestionario";
import Preguntas from "./../auxCuestionario";

const line = "#e9e9e9";

const styles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  grid: {
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: line
  },
  titleLabel: {
    backgroundColor: "#f3f3f3"
  },
  innerDescription: {
    padding: theme.spacing(2)
  }
}));

const Vulnerabilidad = () => {
  const classes = styles();

  return (
    <Fragment>
      <Grid item xs={12} className={classes.titleLabel}>
        <Typography>Principales problemas de la finca ganaderas</Typography>
      </Grid>
      <Grid item xs={12}>
        <Grid container spacing={2}>
          <Preguntas cuestionario={cuestionario.problemas} />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default Vulnerabilidad;
