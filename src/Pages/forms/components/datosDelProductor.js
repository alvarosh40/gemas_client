import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  MenuItem,
  Typography,
  Grid,
  Button,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
} from '@material-ui/core';

import cuestionario from '../cuestionario';
import Preguntas from '../auxCuestionario';

const line = '#e9e9e9';

const styles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  grid: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: line,
  },
  titleLabel: {
    backgroundColor: '#f3f3f3',
  },
  innerDescription: {
    padding: theme.spacing(2),
  },
}));

const DatosDelProductor = () => {
  const classes = styles();

  const { datosDelProductor } = cuestionario;

  return (
    <>
      <Grid item xs={12} className={classes.titleLabel}>
        <Typography>Datos del Productor</Typography>
      </Grid>
      <Grid item xs={12}>
        <Grid container spacing={2}>
          <Preguntas cuestionario={datosDelProductor} />
        </Grid>
      </Grid>
    </>
  );
};

export default DatosDelProductor;
