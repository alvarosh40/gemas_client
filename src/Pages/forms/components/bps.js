import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { MenuItem, Typography, Grid, Button } from "@material-ui/core";
import {
  ValidatorForm,
  TextValidator,
  SelectValidator
} from "react-material-ui-form-validator";

import cuestionario from "./../cuestionario";
import Preguntas from "./../auxCuestionario";

const line = "#e9e9e9";

const styles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  grid: {
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: line
  },
  titleLabel: {
    backgroundColor: "#f3f3f3"
  },
  innerDescription: {
    padding: theme.spacing(2)
  }
}));

const BPS = () => {
  const classes = styles();
  const { medidasDeAdaptacion } = cuestionario;
  return (
    <Fragment>
      <Grid item xs={12} className={classes.titleLabel}>
        <Typography>BP's</Typography>
      </Grid>
      <Grid item xs={12}>
        <Grid container spacing={2}>
          <Preguntas cuestionario={medidasDeAdaptacion} />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default BPS;
