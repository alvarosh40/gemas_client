import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Grid, Typography } from '@material-ui/core';

import cuestionarios from '../../utils/cuestionarios';
import Preguntas from './auxCuestionario';

// context
const line = '#e9e9e9';

const styles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  content: {
    padding: theme.spacing(2),
  },
  grid: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: line,
  },
  titleLabel: {
    backgroundColor: '#f3f3f3',
  },
  innerDescription: {
    padding: theme.spacing(2),
  },
}));

const TitleForm = props => {
  const classes = styles();

  return (
    <Typography className={classes.titleFrom} variant="h5">
      {props.children}
    </Typography>
  );
};

const FormDatosDelProductor = props => {
  const { rowData, readOnly, handleOnSubmit } = props;
  const classes = styles();
  const haveData = Object.keys(rowData).length === 0;

  const [_datosDelProductor, setDatosDelProductor] = useState({
    nombreProductor: haveData ? '' : rowData.nombreProductor,
    cedula: haveData ? '' : rowData.cedula,
    sexo: haveData ? '' : rowData.sexo,
    edad: haveData ? '' : rowData.edad,
    estadoCivil: haveData ? '' : rowData.estadoCivil,
    escolaridad: haveData ? '' : rowData.escolaridad,
    provincia: haveData ? '' : rowData.provincia,
    distrito: haveData ? '' : rowData.distrito,
    corregimiento: haveData ? '' : rowData.corregimiento,
    lugar: haveData ? '' : rowData.lugar,
    viveEnFinca: haveData ? '' : rowData.viveEnFinca,
    dondeVive: haveData ? '' : rowData.dondeVive,
    dependientesMenores: haveData ? '' : rowData.dependientesMenores,
    dependientesMayores: haveData ? '' : rowData.dependientesMayores,
  });

  const handleOnChangeDatosDelProductor = event => {
    setDatosDelProductor({
      ..._datosDelProductor,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} className={classes.titleLabel}>
        <TitleForm>Datos del Productor</TitleForm>
      </Grid>
      <Grid item xs={12}>
        <Grid container spacing={2}>
          <Preguntas
            cuestionario={cuestionarios.datosDelProductor}
            values={_datosDelProductor}
            readOnly={readOnly}
            handleOnChange={handleOnChangeDatosDelProductor}
          />
        </Grid>
      </Grid>
      <Grid item xs={12}>
        {readOnly === false ? (
          <div>
            <Button
              variant="contained"
              onClick={() => {
                const { _id } = rowData;
                const data = {
                  id: haveData ? '' : _id,
                  ..._datosDelProductor,
                };
                handleOnSubmit(data);
              }}
            >
              Guardar
            </Button>
          </div>
        ) : (
          <div />
        )}
      </Grid>
    </Grid>
  );
};

export default FormDatosDelProductor;
