import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  Grid,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
} from '@material-ui/core';

const styles = makeStyles(theme => ({
  titleLabel: {
    backgroundColor: '#f3f3f3',
  },
  innerDescription: {
    padding: theme.spacing(2),
  },
}));

const GetCuestionario = props => {
  const { cuestionario, values, readOnly, handleOnChange } = props;
  const classes = styles();

  const SectionPregunta = (element, index) => (
    <Grid key={`opt_${element.name}_${index}`} item xs={12} sm={element.space}>
      <div className={classes.innerDescription}>
        <Grid container spacing={2}>
          <Grid item xs={12} className={classes.titleLabel}>
            <Typography>{element.label}</Typography>
          </Grid>
          {element.content.map((content, i) =>
            GetTypePregunta(content.type, i, content),
          )}
        </Grid>
      </div>
    </Grid>
  );

  const GetTypePregunta = (type, index, element) => {
    switch (type) {
      case 'string':
        return (
          <Grid
            key={`opt_${element.name}_${index}`}
            item
            xs={12}
            sm={element.space}
          >
            <TextField
              name={element.name}
              value={values[element.name]}
              onChange={handleOnChange}
              fullWidth
              variant="outlined"
              label={element.label}
              inputProps={{
                readOnly,
              }}
            />
          </Grid>
        );
      case 'number':
        return (
          <Grid
            key={`opt_${element.name}_${index}`}
            item
            xs={12}
            sm={element.space}
          >
            <TextField
              name={element.name}
              value={values[element.name]}
              onChange={handleOnChange}
              fullWidth
              type="number"
              variant="outlined"
              label={element.label}
              inputProps={{
                readOnly,
              }}
            />
          </Grid>
        );
      case 'radio':
        return (
          <Grid
            key={`opt_${element.name}_${index}`}
            item
            xs={12}
            sm={element.space}
          >
            <FormControl>
              <FormLabel>{element.label}</FormLabel>
              <RadioGroup
                name={element.name}
                value={values[element.name]}
                onChange={handleOnChange}
              >
                {element.select.map((select, index) => (
                  <FormControlLabel
                    key={`opt_select_${select.label}_${index}`}
                    value={select.value}
                    control={<Radio />}
                    disabled={readOnly}
                    label={select.label}
                  />
                ))}
              </RadioGroup>
            </FormControl>
          </Grid>
        );
      case 'section':
        return SectionPregunta(element, index);
      default:
        break;
    }
  };

  return (
    <>
      {cuestionario.map((element, index) =>
        GetTypePregunta(element.type, index, element),
      )}
    </>
  );
};

export default GetCuestionario;
