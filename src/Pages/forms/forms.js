import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Grid, Typography } from '@material-ui/core';

// forms

import cuestionarios from '../../utils/cuestionarios';
import Preguntas from './auxCuestionario';

// context
const line = '#e9e9e9';

const styles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  content: {
    padding: theme.spacing(2),
  },
  grid: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: line,
  },
  titleLabel: {
    backgroundColor: '#f3f3f3',
  },
  innerDescription: {
    padding: theme.spacing(2),
  },
}));

const TitleForm = props => {
  const classes = styles();

  return (
    <Typography className={classes.titleFrom} variant="h5">
      {props.children}
    </Typography>
  );
};

const FormPage = props => {
  const classes = styles();

  const { rowData, readOnly, handleSubmit } = props;

  const haveData = Object.keys(rowData).length === 0;

  const [_datosDeLaFinca, setDatosDeLaFinca] = useState({
    nombreDeLaFinca: haveData ? '' : rowData.nombreDeLaFinca,
    superficieTotal: haveData ? '' : rowData.superficieTotal,
    provincia: haveData ? '' : rowData.provincia,
    distrito: haveData ? '' : rowData.distrito,
    corregimiento: haveData ? '' : rowData.corregimiento,
    lugar: haveData ? '' : rowData.lugar,
    desde_agenciaDelMIDA: haveData ? '' : rowData.desde_agenciaDelMIDA,
    ruta: haveData ? '' : rowData.ruta,
    tiempoDeRecorrido: haveData ? '' : rowData.tiempoDeRecorrido,
    acceso: haveData ? '' : rowData.acceso,
    condicionDeLaVia: haveData ? '' : rowData.condicionDeLaVia,
    medioDeTransporte: haveData ? '' : rowData.medioDeTransporte,
    topo_plana: haveData ? '' : rowData.topo_plana,
    topo_ondulada: haveData ? '' : rowData.topo_ondulada,
    topo_quebrada: haveData ? '' : rowData.topo_quebrada,
    colindante_norte: haveData ? '' : rowData.colindante_norte,
    colindante_sur: haveData ? '' : rowData.colindante_sur,
    colindante_este: haveData ? '' : rowData.colindante_este,
    colindante_oeste: haveData ? '' : rowData.colindante_oeste,
    coordenada1_x: haveData ? '' : rowData.coordenada1_x,
    coordenada1_y: haveData ? '' : rowData.coordenada1_y,
    coordenada2_x: haveData ? '' : rowData.coordenada2_x,
    coordenada2_y: haveData ? '' : rowData.coordenada2_y,
    tenencia: haveData ? '' : rowData.tenencia,
    numeroDeTitulo: haveData ? '' : rowData.numeroDeTitulo,
    dondeAguaConsumoHumano: haveData ? '' : rowData.dondeAguaConsumoHumano,
    dondeAguaFincaProduccion: haveData ? '' : rowData.dondeAguaFincaProduccion,
    accesoAElectricidad: haveData ? '' : rowData.accesoAElectricidad,
    usoDePanelesSolares: haveData ? '' : rowData.usoDePanelesSolares,
    paraQueUsoPanelesSolares: haveData ? '' : rowData.paraQueUsoPanelesSolares,
  });

  const [_recursosNaturales, setRecursosNaturales] = useState({
    ap_dentro: haveData ? '' : rowData.ap_dentro,
    ap_zonaDeAmortiguamiento: haveData ? '' : rowData.ap_zonaDeAmortiguamiento,
    ap_nombreDelAreaProtegida: haveData
      ? ''
      : rowData.ap_nombreDelAreaProtegida,
    recursoHidrico_rio_nombre: haveData
      ? ''
      : rowData.recursoHidrico_rio_nombre,
    recursoHidrico_rio_permanente: haveData
      ? ''
      : rowData.recursoHidrico_rio_permanente,
    recursoHidrico_rio_temporal: haveData
      ? ''
      : rowData.recursoHidrico_rio_temporal,
    recursoHidrico_quebrada: haveData ? '' : rowData.recursoHidrico_quebrada,
    recursoHidrico_quebrada_permanente: haveData
      ? ''
      : rowData.recursoHidrico_quebrada_permanente,
    recursoHidrico_quebrada_temporal: haveData
      ? ''
      : rowData.recursoHidrico_quebrada_temporal,
    bosque_maduros: haveData ? '' : rowData.bosque_maduros,
    bosque_secundarios: haveData ? '' : rowData.bosque_secundarios,
    plantacion_SiNo: haveData ? '' : rowData.plantacion_SiNo,
    bosque_especiesNativasDominantesEnFinca: haveData
      ? ''
      : rowData.bosque_especiesNativasDominantesEnFinca,
    bosque_usoDelasEspeciesNativas: haveData
      ? ''
      : rowData.bosque_usoDelasEspeciesNativas,
    bosque_numero_de_arbolesNativosEnPotrero: haveData
      ? ''
      : rowData.bosque_numero_de_arbolesNativosEnPotrero,
    suelo_tipo_pedregoso: haveData ? '' : rowData.suelo_tipo_pedregoso,
    suelo_tipo_arenoso: haveData ? '' : rowData.suelo_tipo_arenoso,
    suelo_tipo_franco: haveData ? '' : rowData.suelo_tipo_franco,
    suelo_tipo_arcilloso: haveData ? '' : rowData.suelo_tipo_arcilloso,
    suelo_condicion_erosionado: haveData
      ? ''
      : rowData.suelo_condicion_erosionado,
    suelo_condicion_degradado: haveData
      ? ''
      : rowData.suelo_condicion_degradado,
    suelo_condicion_desnudoExpuesto: haveData
      ? ''
      : rowData.suelo_condicion_desnudoExpuesto,
  });

  const [_ganaderia, setGanaderia] = useState({
    sistemaProductivo: haveData ? '' : rowData.sistemaProductivo,
    tipo_ganaderia_carne: haveData ? '' : rowData.tipo_ganaderia_carne,
    tipo_ganaderia_carne_raza: haveData
      ? ''
      : rowData.tipo_ganaderia_carne_raza,
    tipo_ganaderia_leche: haveData ? '' : rowData.tipo_ganaderia_leche,
    tipo_ganaderia_leche_raza: haveData
      ? ''
      : rowData.tipo_ganaderia_leche_raza,
    tipo_ganaderia_mixto: haveData ? '' : rowData.tipo_ganaderia_mixto,
    tipo_ganaderia_mixto_raza: haveData
      ? ''
      : rowData.tipo_ganaderia_mixto_raza,
    pasto_tipo_nativo_ha: haveData ? '' : rowData.pasto_tipo_nativo_ha,
    pasto_tipo_mejorado_ha: haveData ? '' : rowData.pasto_tipo_mejorado_ha,
    pasto_total_ha: haveData ? '' : rowData.pasto_total_ha,
    pasto_especie_nativo: haveData ? '' : rowData.pasto_especie_nativo,
    pasto_especie_mejorado: haveData ? '' : rowData.pasto_especie_mejorado,
    pasto_frecuencia_de_rotacion_dias: haveData
      ? ''
      : rowData.pasto_frecuencia_de_rotacion_dias,
    divisionDePotreros_cantidad: haveData
      ? ''
      : rowData.divisionDePotreros_cantidad,
    divisionDePotreros_areaPromedio_ha: haveData
      ? ''
      : rowData.divisionDePotreros_areaPromedio_ha,
    cercasVivas_B: haveData ? '' : rowData.cercasVivas_B,
    cercasVivas_especies: haveData ? '' : rowData.cercasVivas_especies,
    bancosForajeros_B: haveData ? '' : rowData.bancosForajeros_B,
    bancosForrajeros_especiesUsadas: haveData
      ? ''
      : rowData.bancosForrajeros_especiesUsadas,
    bancosForrajeros_ensilaje: haveData
      ? ''
      : rowData.bancosForrajeros_ensilaje,
    arbolesDispersosEnPotreros: haveData
      ? ''
      : rowData.arbolesDispersosEnPotreros,
    estructuraDeLafinca_Casa: haveData ? '' : rowData.estructuraDeLafinca_Casa,
    estructuraDeLafinca_corral: haveData
      ? ''
      : rowData.estructuraDeLafinca_corral,
    estructuraDeLaFinca_corral_tipo: haveData
      ? ''
      : rowData.estructuraDeLaFinca_corral_tipo,
    estructuraDeLaFinca_breteChutra: haveData
      ? ''
      : rowData.estructuraDeLaFinca_breteChutra,
    estructuraDeLaFinca_cercasEléctricas: haveData
      ? ''
      : rowData.estructuraDeLaFinca_cercasEléctricas,
    estructuraDeLaFinca_galeras: haveData
      ? ''
      : rowData.estructuraDeLaFinca_galeras,
    estructuraDeLaFinca_chiqueros: haveData
      ? ''
      : rowData.estructuraDeLaFinca_chiqueros,
    estructuraDeLaFinca_acueductoDeLaFinca: haveData
      ? ''
      : rowData.estructuraDeLaFinca_acueductoDeLaFinca,
    estructuraDeLaFinca_bebederosFijos: haveData
      ? ''
      : rowData.estructuraDeLaFinca_bebederosFijos,
    estructuraDeLaFinca_bebederosMoviles: haveData
      ? ''
      : rowData.estructuraDeLaFinca_bebederosMoviles,
    estructuraDeLaFinca_cosechaDeAgua: haveData
      ? ''
      : rowData.estructuraDeLaFinca_cosechaDeAgua,
    estructuraDeLaFinca_pozos: haveData
      ? ''
      : rowData.estructuraDeLaFinca_pozos,
    estructuraDeLaFinca_reservoriosLagos: haveData
      ? ''
      : rowData.estructuraDeLaFinca_reservoriosLagos,
    estructuraDeLaFinca_sistemaDeRiego: haveData
      ? ''
      : rowData.estructuraDeLaFinca_sistemaDeRiego,
    estructuraDeLaFincaOtrosCuales: haveData
      ? ''
      : rowData.estructuraDeLaFincaOtrosCuales,
    inventario_vacasParidas: haveData ? '' : rowData.inventario_vacasParidas,
    inventario_vacasPreñadas: haveData ? '' : rowData.inventario_vacasPreñadas,
    inventario_vacasSecas: haveData ? '' : rowData.inventario_vacasSecas,
    inventario_novillasAdultas: haveData
      ? ''
      : rowData.inventario_novillasAdultas,
    inventario_novillasDesarrollo: haveData
      ? ''
      : rowData.inventario_novillasDesarrollo,
    inventario_terneras: haveData ? '' : rowData.inventario_terneras,
    inventario_terneros: haveData ? '' : rowData.inventario_terneros,
    inventario_novillosDesarrollo: haveData
      ? ''
      : rowData.inventario_novillosDesarrollo,
    inventario_novillosDeCeba: haveData
      ? ''
      : rowData.inventario_novillosDeCeba,
    inventario_sementales: haveData ? '' : rowData.inventario_sementales,
    inventario_caballos: haveData ? '' : rowData.inventario_caballos,
    inventario_otrasEspecies: haveData ? '' : rowData.inventario_otrasEspecies,
    inventario_cerdos: haveData ? '' : rowData.inventario_cerdos,
    inventario_caprinos: haveData ? '' : rowData.inventario_caprinos,
    inventario_aves: haveData ? '' : rowData.inventario_aves,
    inventario_total: haveData ? '' : rowData.inventario_total,
    comercializacion_ganado_aQuienLeVende: haveData
      ? ''
      : rowData.comercializacion_ganado_aQuienLeVende,
    comercializacion_ganado_DondeVende: haveData
      ? ''
      : rowData.comercializacion_ganado_DondeVende,
    comercializacion_ganado_cuandoVende: haveData
      ? ''
      : rowData.comercializacion_ganado_cuandoVende,
  });

  const [_agricultura, setAgricultura] = useState({
    agricultura_tipo_subsistencia_b: haveData
      ? ''
      : rowData.agricultura_tipo_subsistencia_b,
    agricultura_tipo_subsistencia_superficie_ha: haveData
      ? ''
      : rowData.agricultura_tipo_subsistencia_superficie_ha,
    agricultura_tipo_subsistencia_rubros: haveData
      ? ''
      : rowData.agricultura_tipo_subsistencia_rubros,
    agricultura_tipo_comercial_b: haveData
      ? ''
      : rowData.agricultura_tipo_comercial_b,
    agricultura_tipo_comercial_superficie_ha: haveData
      ? ''
      : rowData.agricultura_tipo_comercial_superficie_ha,
    agricultura_tipo_comercial_rubros: haveData
      ? ''
      : rowData.agricultura_tipo_comercial_rubros,
    agricultura_tieneSistemaDeRiesgo_b: haveData
      ? ''
      : rowData.agricultura_tieneSistemaDeRiesgo_b,
    agricultura_tipoDeRiego: haveData ? '' : rowData.agricultura_tipoDeRiego,
  });

  const [_medidasDeAdaptacion, setMedidasDeAdaptacion] = useState({
    ma_aplicaAlgunTipoDeMedidaDeAdaptacionAlCC: haveData
      ? ''
      : rowData.ma_aplicaAlgunTipoDeMedidaDeAdaptacionAlCC,
    ma_usaTecnologia: haveData ? '' : rowData.ma_usaTecnologia,
    ma_queBeneficiosLeHaTraido: haveData
      ? ''
      : rowData.ma_queBeneficiosLeHaTraido,
    ma_laReplicaria: haveData ? '' : rowData.ma_laReplicaria,
    si_no_opcion_recursoEconomico: haveData
      ? ''
      : rowData.si_no_opcion_recursoEconomico,
    si_no_opcion_asistenciaTecnica: haveData
      ? ''
      : rowData.si_no_opcion_asistenciaTecnica,
    si_no_opcion_noCreeEnEllas: haveData
      ? ''
      : rowData.si_no_opcion_noCreeEnEllas,
    si_no_noLeInteresa: haveData ? '' : rowData.si_no_noLeInteresa,
    si_no_opcion_otra: haveData ? '' : rowData.si_no_opcion_otra,
    ma_aplicadas_estructurales: haveData
      ? ''
      : rowData.ma_aplicadas_estructurales,
    ma_aplicada_noEstructurales: haveData
      ? ''
      : rowData.ma_aplicada_noEstructurales,
  });

  const [_asistenciaTécnicaYCredito, setAsistenciaTécnicaYCredito] = useState({
    acceso_asistenciaTecnica_recibe: haveData
      ? ''
      : rowData.acceso_asistenciaTecnica_recibe,
    acceso_asistenciaTecnica_si_si_quienes: haveData
      ? ''
      : rowData.acceso_asistenciaTecnica_si_si_quienes,
    acceso_asistenciaTecnica_si_si_QuienesOtra: haveData
      ? ''
      : rowData.acceso_asistenciaTecnica_si_si_QuienesOtra,
    acceso_asistenciaTecnica_frecuenciaDeLaVisitas: haveData
      ? ''
      : rowData.acceso_asistenciaTecnica_frecuenciaDeLaVisitas,
    acceso_trabajaConPrestamoAgropecuario: haveData
      ? ''
      : rowData.acceso_trabajaConPrestamoAgropecuario,
    acceso_prestamoAgropecuario_BDA: haveData
      ? ''
      : rowData.acceso_prestamoAgropecuario_BDA,
    acceso_prestamoAgropecuario_BNP: haveData
      ? ''
      : rowData.acceso_prestamoAgropecuario_BNP,
    acceso_prestamoAgropecuario_Otro_cual: haveData
      ? ''
      : rowData.acceso_prestamoAgropecuario_Otro_cual,
    acceso_prestamoAgropecuario_paraQueEsElFincanciamiento: haveData
      ? ''
      : rowData.acceso_prestamoAgropecuario_paraQueEsElFincanciamiento,
  });

  const [_asociatividad, setAsociatividad] = useState({
    pertenece_grupo_asociacion_coperativaDeProductores: haveData
      ? ''
      : rowData.pertenece_grupo_asociacion_coperativaDeProductores,
    pertenece_grupo_asociacion_coperativaDeProductores_cual: haveData
      ? ''
      : rowData.pertenece_grupo_asociacion_coperativaDeProductores_cual,
    estaActivo: haveData ? '' : rowData.estaActivo,
    porqueNoEstaActivo: haveData ? '' : rowData.porqueNoEstaActivo,
    queBeneficiosLeAhTraidoPerteneceraEstaOrganizacion: haveData
      ? ''
      : rowData.queBeneficiosLeAhTraidoPerteneceraEstaOrganizacion,
    queMotivaAlaGenteDeAquiAParticiparEnActividadesGrupales: haveData
      ? ''
      : rowData.queMotivaAlaGenteDeAquiAParticiparEnActividadesGrupales,
  });

  const [_participacion, setParticipacion] = useState({
    participacion_actividad_talleres: haveData
      ? ''
      : rowData.participacion_actividad_talleres,
    participacion_actividad_diasDeCampo: haveData
      ? ''
      : rowData.participacion_actividad_diasDeCampo,
    participacion_actividad_charlas: haveData
      ? ''
      : rowData.participacion_actividad_charlas,
    participacion_actividad_intercambios: haveData
      ? ''
      : rowData.participacion_actividad_intercambios,
    participacion_actividad_otrosCuales: haveData
      ? ''
      : rowData.participacion_actividad_otrosCuales,
    m_h_participacionDeActividades: haveData
      ? ''
      : rowData.m_h_participacionDeActividades,
    m_h_participacionDeActividades_porque: haveData
      ? ''
      : rowData.m_h_participacionDeActividades_porque,
    comoSePuedeIncorporarALosJovenesEnLasActividadesDeCapacitacion: haveData
      ? ''
      : rowData.comoSePuedeIncorporarALosJovenesEnLasActividadesDeCapacitacion,
  });

  const [_conocimientoDelTema, setConocimientoDelTema] = useState({
    conocimiento_cambioClimatico: haveData
      ? ''
      : rowData.conocimiento_cambioClimatico,
    conocimiento_resiliencia: haveData ? '' : rowData.conocimiento_resiliencia,
    conocimiento_ganaderiaSostenible: haveData
      ? ''
      : rowData.conocimiento_ganaderiaSostenible,
    conocimiento_sistemaSilvopastoriles: haveData
      ? ''
      : rowData.conocimiento_sistemaSilvopastoriles,
    conocimiento_medidasDeAdaptacion: haveData
      ? ''
      : rowData.conocimiento_medidasDeAdaptacion,
  });

  const [_problemas, setProblemas] = useState({
    produccionAgropecuaria_sistemasDeRegistro: haveData
      ? ''
      : rowData.produccionAgropecuaria_sistemasDeRegistro,
    produccionAgropecuaria_tamañoDeLosPotreros: haveData
      ? ''
      : rowData.produccionAgropecuaria_tamañoDeLosPotreros,
    produccionAgropecuaria_disponibilidadDeAgua: haveData
      ? ''
      : rowData.produccionAgropecuaria_disponibilidadDeAgua,
    produccionAgropecuaria_ofertasDeForraje: haveData
      ? ''
      : rowData.produccionAgropecuaria_ofertasDeForraje,
    produccionAgropecuaria_seguridadAlimentaria: haveData
      ? ''
      : rowData.produccionAgropecuaria_seguridadAlimentaria,
    serviciosAmbientales_proteccionDeFuentesDeAgua: haveData
      ? ''
      : rowData.serviciosAmbientales_proteccionDeFuentesDeAgua,
    serviciosAmbientales_conservacionDeSuelos: haveData
      ? ''
      : rowData.serviciosAmbientales_conservacionDeSuelos,
    serviciosAmbientales_coberturaDeArbolesEnPotrero: haveData
      ? ''
      : rowData.serviciosAmbientales_coberturaDeArbolesEnPotrero,
    serviciosAmbientales_riquezaDeArbolesEnLosPotreros: haveData
      ? ''
      : rowData.serviciosAmbientales_riquezaDeArbolesEnLosPotreros,
    serviciosAmbientales_cercasVivas: haveData
      ? ''
      : rowData.serviciosAmbientales_cercasVivas,
    serviciosAmbientales_usoDeAgroquimicos: haveData
      ? ''
      : rowData.serviciosAmbientales_usoDeAgroquimicos,
    conocimientoInformacion_cambioClimatico: haveData
      ? ''
      : rowData.conocimientoInformacion_cambioClimatico,
    conocimientoInformacion_medidasDeAdaptacion: haveData
      ? ''
      : rowData.conocimientoInformacion_medidasDeAdaptacion,
    manejoDeLaFinca_sistemaDeRegistros: haveData
      ? ''
      : rowData.manejoDeLaFinca_sistemaDeRegistros,
    manejoDeLaFinca_pastoreo: haveData ? '' : rowData.manejoDeLaFinca_pastoreo,
    manejoDeLaFinca_pocaCoberturaArborea: haveData
      ? ''
      : rowData.manejoDeLaFinca_pocaCoberturaArborea,
    manejoDeLaFinca_disponibilidadDeAgua: haveData
      ? ''
      : rowData.manejoDeLaFinca_disponibilidadDeAgua,
    manejoDeLaFinca_accesoDisponibilidadDistribucionDeAguaAlPotrero: haveData
      ? ''
      : rowData.manejoDeLaFinca_accesoDisponibilidadDistribucionDeAguaAlPotrero,
    manejoDeLaFinca_sedimentacionDeLasFuentesHidricas: haveData
      ? ''
      : rowData.manejoDeLaFinca_sedimentacionDeLasFuentesHidricas,
    manejoDeLaFinca_erosionDelSuelo: haveData
      ? ''
      : rowData.manejoDeLaFinca_erosionDelSuelo,
    manejoDelHato_saludAnimal: haveData
      ? ''
      : rowData.manejoDelHato_saludAnimal,
    manejoDelHato_genetica: haveData ? '' : rowData.manejoDelHato_genetica,
    manejoDelHato_rendimiento: haveData
      ? ''
      : rowData.manejoDelHato_rendimiento,
    manejoDelHato_categorizacionDelGanado: haveData
      ? ''
      : rowData.manejoDelHato_categorizacionDelGanado,
    manejoDelHato_programacionDeMonta: haveData
      ? ''
      : rowData.manejoDelHato_programacionDeMonta,
    manejoDelHato_mortalidadEstacional: haveData
      ? ''
      : rowData.manejoDelHato_mortalidadEstacional,
    manejoDelHato_nutricion: haveData ? '' : rowData.manejoDelHato_nutricion,
    tecnologiaAplicada_ensilaje_henificacion: haveData
      ? ''
      : rowData.tecnologiaAplicada_ensilaje_henificacion,
    tecnologiaAplicada_suplementacion: haveData
      ? ''
      : rowData.tecnologiaAplicada_suplementacion,
    tecnologiaAplicada_preparacionDeSuelo: haveData
      ? ''
      : rowData.tecnologiaAplicada_preparacionDeSuelo,
    tecnologiaAplicada_divisionDePotreros: haveData
      ? ''
      : rowData.tecnologiaAplicada_divisionDePotreros,
    conocimientoEInformacion_cambioClimatico: haveData
      ? ''
      : rowData.conocimientoEInformacion_cambioClimatico,
    conocimientoEInformacion_medidasDeAdaptacion: haveData
      ? ''
      : rowData.conocimientoEInformacion_medidasDeAdaptacion,
    problemas_otros: haveData ? '' : rowData.problemas_otros,
  });

  const handleOnChangeDatosDeLaFinca = e => {
    setDatosDeLaFinca({
      ..._datosDeLaFinca,
      [e.target.name]: e.target.value,
    });
  };

  const handleOnChangeRecursosNaturales = e => {
    setRecursosNaturales({
      ..._recursosNaturales,
      [e.target.name]: e.target.value,
    });
  };

  const handleOnChangeGanaderia = e => {
    setGanaderia({
      ..._ganaderia,
      [e.target.name]: e.target.value,
    });
  };

  const handleOnChangeAgricultura = e => {
    setAgricultura({
      ..._agricultura,
      [e.target.name]: e.target.value,
    });
  };

  const handleOnChangeMedidasDeAdaptacion = e => {
    setMedidasDeAdaptacion({
      ..._medidasDeAdaptacion,
      [e.target.name]: e.target.value,
    });
  };

  const handleOnChangeAsistenciaTecnicaYCredito = e => {
    setAsistenciaTécnicaYCredito({
      ..._asistenciaTécnicaYCredito,
      [e.target.name]: e.target.value,
    });
  };

  const handleOnChangeAsociatividad = e => {
    setAsociatividad({
      ..._asociatividad,
      [e.target.name]: e.target.value,
    });
  };

  const handleOnChangeParticipacion = e => {
    setParticipacion({
      ..._participacion,
      [e.target.name]: e.target.value,
    });
  };

  const handleOnChangeConocimientoDelTema = e => {
    setConocimientoDelTema({
      ..._conocimientoDelTema,
      [e.target.name]: e.target.value,
    });
  };

  const handleOnChangeProblemas = e => {
    setProblemas({
      ..._problemas,
      [e.target.name]: e.target.value,
    });
  };

  const {
    datosDelProductor,
    datosDeLaFinca,
    recursosNaturales,
    ganaderia,
    agricultura,
    medidasDeAdaptacion,
    asistenciaTécnicaYCredito,
    asociatividad,
    participacion,
    conocimientoDelTema,
    problemas,
  } = cuestionarios;

  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Grid container spacing={2} className={classes.grid}>
            <Grid item xs={12} className={classes.titleLabel}>
              <TitleForm>Datos generales de la finca</TitleForm>
            </Grid>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Preguntas
                  cuestionario={datosDeLaFinca}
                  values={_datosDeLaFinca}
                  readOnly={readOnly}
                  handleOnChange={handleOnChangeDatosDeLaFinca}
                />
              </Grid>
            </Grid>

            <Grid item xs={12} className={classes.titleLabel}>
              <TitleForm>Recursos de la finca</TitleForm>
            </Grid>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Preguntas
                  cuestionario={recursosNaturales}
                  values={_recursosNaturales}
                  readOnly={readOnly}
                  handleOnChange={handleOnChangeRecursosNaturales}
                />
              </Grid>
            </Grid>

            <Grid item xs={12} className={classes.titleLabel}>
              <TitleForm>Ganaderia</TitleForm>
            </Grid>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Preguntas
                  cuestionario={ganaderia}
                  values={_ganaderia}
                  readOnly={readOnly}
                  handleOnChange={handleOnChangeGanaderia}
                />
              </Grid>
            </Grid>

            <Grid item xs={12} className={classes.titleLabel}>
              <TitleForm>Agricultura</TitleForm>
            </Grid>
            <Grid item xs={12} className={classes.grid}>
              <Grid container spacing={2}>
                <Preguntas
                  cuestionario={agricultura}
                  values={_agricultura}
                  readOnly={readOnly}
                  handleOnChange={handleOnChangeAgricultura}
                />
              </Grid>
            </Grid>

            <Grid item xs={12} className={classes.titleLabel}>
              <TitleForm>Medidas de adaptación</TitleForm>
            </Grid>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Preguntas
                  cuestionario={medidasDeAdaptacion}
                  values={_medidasDeAdaptacion}
                  readOnly={readOnly}
                  handleOnChange={handleOnChangeMedidasDeAdaptacion}
                />
              </Grid>
            </Grid>

            <Grid item xs={12} className={classes.titleLabel}>
              <TitleForm>Asistencia tecnica y credito</TitleForm>
            </Grid>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Preguntas
                  cuestionario={asistenciaTécnicaYCredito}
                  values={_asistenciaTécnicaYCredito}
                  readOnly={readOnly}
                  handleOnChange={handleOnChangeAsistenciaTecnicaYCredito}
                />
              </Grid>
            </Grid>

            <Grid item xs={12} className={classes.titleLabel}>
              <TitleForm>Asociatividad</TitleForm>
            </Grid>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Preguntas
                  cuestionario={asociatividad}
                  values={_asociatividad}
                  readOnly={readOnly}
                  handleOnChange={handleOnChangeAsociatividad}
                />
              </Grid>
            </Grid>

            <Grid item xs={12} className={classes.titleLabel}>
              <TitleForm>Participacion</TitleForm>
            </Grid>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Preguntas
                  cuestionario={participacion}
                  values={_participacion}
                  readOnly={readOnly}
                  handleOnChange={handleOnChangeParticipacion}
                />
              </Grid>
            </Grid>

            <Grid item xs={12} className={classes.titleLabel}>
              <TitleForm>Conocimiento del tema</TitleForm>
            </Grid>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Preguntas
                  cuestionario={conocimientoDelTema}
                  values={_conocimientoDelTema}
                  readOnly={readOnly}
                  handleOnChange={handleOnChangeConocimientoDelTema}
                />
              </Grid>
            </Grid>

            <Grid item xs={12} className={classes.titleLabel}>
              <TitleForm>Principales problemas de la finca ganaderas</TitleForm>
            </Grid>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Preguntas
                  cuestionario={problemas}
                  values={_problemas}
                  readOnly={readOnly}
                  handleOnChange={handleOnChangeProblemas}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          {readOnly === false ? (
            <div>
              <Button
                variant="contained"
                onClick={() => {
                  const { _id } = rowData;
                  const data = {
                    id: haveData ? '' : _id,
                    ..._datosDeLaFinca,
                    ..._recursosNaturales,
                    ..._ganaderia,
                    ..._agricultura,
                    ..._medidasDeAdaptacion,
                    ..._asistenciaTécnicaYCredito,
                    ..._asociatividad,
                    ..._participacion,
                    ..._conocimientoDelTema,
                    ..._problemas,
                  };
                  handleSubmit(data);
                }}
              >
                Guardar
              </Button>
            </div>
          ) : (
            <div />
          )}
        </Grid>
      </Grid>
    </div>
  );
};

export default FormPage;
