import React, { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  Avatar,
  TextField,
  CssBaseline,
  Button,
} from '@material-ui/core';
import { LockOutlined as LockOutlinedIcon } from '@material-ui/icons';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import AuthContext from '../../contexts/authContext';

const styles = makeStyles(theme => ({
  root: {},
  paper: {
    margin: `${theme.spacing(8)}px ${theme.spacing(2)}px ${theme.spacing(
      2,
    )}px ${theme.spacing(2)}px`,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Login = props => {
  const { closeDialog } = props;
  const classes = styles();
  const [payload, setPayload] = useState({
    username: '',
    password: '',
  });
  const authContext = useContext(AuthContext);
  const { handleLogin } = authContext;

  const handleOnChange = e => {
    setPayload({
      ...payload,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Iniciar sesion
        </Typography>
        <ValidatorForm
          noValidate
          onSubmit={async () => {
            const res = handleLogin(payload);
            if (res) {
              closeDialog();
            }
          }}
        >
          <TextValidator
            variant="outlined"
            margin="normal"
            validators={['required']}
            errorMessages={['Este campo no puede estar vacio']}
            required
            fullWidth
            value={payload.username}
            onChange={handleOnChange}
            id="username"
            label="Usuario"
            name="username"
            autoComplete="username"
          />
          <TextValidator
            variant="outlined"
            margin="normal"
            validators={['required']}
            errorMessages={['Este campo no puede estar vacio']}
            required
            fullWidth
            value={payload.password}
            onChange={handleOnChange}
            name="password"
            label="Contraseña"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Iniciar sesion
          </Button>
        </ValidatorForm>
      </div>
    </>
  );
};

export default Login;
