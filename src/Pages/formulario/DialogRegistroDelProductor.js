import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Dialog,
  Toolbar,
  Button,
  AppBar,
  IconButton,
  Slide,
} from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';

import FormRegistroDelProductor from '../forms/forms';

const styles = makeStyles(theme => ({
  root: {},
  dialogContent: {
    display: 'flex',
    justifyContent: 'center',
  },
  closeIcon: {
    color: theme.palette.common.white,
  },
  actionToolbar: {
    marginLeft: 'auto',
  },
  dialogMain: {
    flexGrow: 1,
    maxWidth: 1020,
    padding: theme.spacing(2),
  },
}));

const Transition = React.forwardRef((props, ref) => (
  <Slide direction="up" ref={ref} {...props} />
));

const DialogRegistroDelProductor = props => {
  const {
    open,
    close,
    setReadOnly,
    setEditData,
    login,
    newData,
    rowData,
    readOnly,
    handleOnSubmit,
  } = props;
  const classes = styles();

  return (
    <Dialog
      fullScreen
      open={open}
      onClose={close}
      TransitionComponent={Transition}
    >
      <AppBar position="relative">
        <Toolbar>
          <IconButton onClick={close}>
            <CloseIcon className={classes.closeIcon} />
          </IconButton>
          <div className={classes.actionToolbar}>
            {login ? (
              newData ? (
                <div />
              ) : (
                <Button
                  variant="contained"
                  onClick={() => {
                    setEditData(true);
                    setReadOnly(false);
                  }}
                >
                  Editar
                </Button>
              )
            ) : (
              <div />
            )}
          </div>
        </Toolbar>
      </AppBar>
      <div className={classes.dialogContent}>
        <div className={classes.dialogMain}>
          <FormRegistroDelProductor
            rowData={rowData}
            readOnly={readOnly}
            handleSubmit={handleOnSubmit}
          />
        </div>
      </div>
    </Dialog>
  );
};

export default DialogRegistroDelProductor;
