import React, { useState, useContext } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import {
  Button,
  Typography,
  Box,
  Table,
  TableRow,
  TableCell,
  TableBody,
  TableHead,
} from '@material-ui/core';
import {
  Visibility as SeeIcon,
  Add as AddIcon,
  Delete as DeleteIcon,
} from '@material-ui/icons';
import moment from 'moment';
import {
  useGetListOfRegisterByProducerID,
  usePostRegistros,
  usePutRegistros,
  useDeleteRegistros,
} from '../../utils/apiHooks';

import AuthContext from '../../contexts/authContext';
import DialogRegistroDelProductor from './DialogRegistroDelProductor';

const styles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2),
    background: '#f6f6f6',
  },
  headerTable: {
    display: 'flex',
  },
  actionHeader: {
    marginLeft: 'auto',
  },
}));

const DetailPanelRegistroDelProductor = props => {
  const { producerID } = props;

  const classes = styles();
  const { login } = useContext(AuthContext);

  const { data, isSuccess, isLoading } = useGetListOfRegisterByProducerID(
    producerID,
  );
  const [postRegistro] = usePostRegistros();
  const [putRegistro] = usePutRegistros();
  const [deleteRegistro] = useDeleteRegistros();

  const [openDialog, setOpenDialog] = useState(false);

  const [rowData, setRowData] = useState({});

  // flow
  const [readOnly, setReadOnly] = useState(false);
  const [editRegistro, setEditRegistro] = useState(false);
  const [newRegistro, setNewRegistro] = useState(false);

  const handleOnClickRow = row => {
    setRowData(row);
    setReadOnly(true);
    setOpenDialog(true);
  };

  const handleOnAddRegistro = () => {
    setNewRegistro(true);
    setReadOnly(false);
    setOpenDialog(true);
  };

  const handleOnCloseDialog = () => {
    setOpenDialog(false);
    setNewRegistro(false);
    setEditRegistro(false);
    setRowData({});
  };

  const handleOnDeleteRow = registroID => {
    const res = window.confirm('Realmente desea borrar esta encuesta?');

    if (res) {
      deleteRegistro(registroID);
    }
  };

  const handleOnSubmitRegistro = async registroData => {
    if (editRegistro) {
      putRegistro(registroData, {
        onError: () => {
          alert('error');
          handleOnCloseDialog();
        },
        onSuccess: () => {
          alert('la encuesta ah sido editado con exito');
          handleOnCloseDialog();
        },
      });
    } else {
      const newRegistroData = {
        productorID: producerID,
        timestamp: moment().format(),
        ...registroData,
      };
      postRegistro(newRegistroData, {
        onError: () => {
          alert('error');
          handleOnCloseDialog();
        },
        onSuccess: () => {
          alert('la encuesta ah sido creada con exito');
          handleOnCloseDialog();
        },
      });
    }
  };

  if (isSuccess) {
    return (
      <div className={classes.root}>
        <DialogRegistroDelProductor
          open={openDialog}
          close={handleOnCloseDialog}
          setReadOnly={setReadOnly}
          setEditData={setEditRegistro}
          login={login}
          newData={newRegistro}
          rowData={rowData}
          readOnly={readOnly}
          handleOnSubmit={handleOnSubmitRegistro}
        />
        <Box margin={1}>
          <div className={classes.headerTable}>
            <Typography variant="h6" gutterBottom component="div">
              Registro de encuestas.
            </Typography>
            <div className={classes.actionHeader}>
              {login ? (
                <Button
                  variant="contained"
                  color="primary"
                  startIcon={<AddIcon />}
                  onClick={() => handleOnAddRegistro()}
                >
                  Agregar una encuesta.
                </Button>
              ) : (
                <div />
              )}
            </div>
          </div>
          <Table size="small" aria-label="purchases">
            <TableHead>
              <TableRow>
                <TableCell>Acciones</TableCell>
                <TableCell>version</TableCell>
                <TableCell>Nombre de la finca</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map(registro => (
                <TableRow key={registro._id} hover>
                  <TableCell component="th" scope="row">
                    {login ? (
                      <Button
                        variant="contained"
                        color="secondary"
                        startIcon={<DeleteIcon />}
                        onClick={() => handleOnDeleteRow(registro._id)}
                      >
                        Borrar
                      </Button>
                    ) : (
                      <div />
                    )}

                    <Button
                      variant="contained"
                      color="primary"
                      startIcon={<SeeIcon />}
                      onClick={() => handleOnClickRow(registro)}
                    >
                      Ver
                    </Button>
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {moment(registro.timestamp).format('DD-MM-YYYY')}
                  </TableCell>
                  <TableCell>{registro.nombreDeLaFinca}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </div>
    );
  }

  if (isLoading) {
    return <div> Loading... </div>;
  }

  return <div />;
};

export default DetailPanelRegistroDelProductor;
