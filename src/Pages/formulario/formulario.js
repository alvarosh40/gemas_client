import React, { useState, forwardRef, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MaterialTable from 'material-table';
import { Slide } from '@material-ui/core';
import Axios from 'axios';
import {
  useGetListOfProductores,
  usePostProductores,
  usePutProductores,
  useDeleteProductores,
  useDeleteRegistros,
} from '../../utils/apiHooks';

import AuthContext from '../../contexts/authContext';
import DialogProductor from './DialogDatosDelProductor';
import DetailPanelRegister from './detailPanelRegistrosDelProductor';

import Config from '../../utils/config';

const styles = makeStyles(theme => ({
  root: {},
  dialogContent: {
    display: 'flex',
    justifyContent: 'center',
  },
  closeIcon: {
    color: theme.palette.common.white,
  },
  actionToolbar: {
    marginLeft: 'auto',
  },
  dialogMain: {
    flexGrow: 1,
    maxWidth: 1020,
    padding: theme.spacing(2),
  },
}));

const GetLookUp = array => {
  const unique = [...new Set(array)];
  const toKeyValue = unique.map(e => ({
    [e]: e,
  }));

  const arrayToObj = toKeyValue.reduce(
    (accum, item) => ({ ...accum, ...item }),
    {},
  );
  return arrayToObj;
};

const Table = () => {
  const authContext = useContext(AuthContext);
  const { login } = authContext;

  const { data, isSuccess, isLoading } = useGetListOfProductores();
  const [postProductores] = usePostProductores();
  const [putProductores] = usePutProductores();
  const [deleteProductores] = useDeleteProductores();
  const [deleteRegistros] = useDeleteRegistros();

  const [openDialogProducer, setOpenDialogProducer] = useState(false);

  const [rowData, setRowData] = useState({});

  // flow producer
  const [editProducer, setEditProducer] = useState(false);
  const [newProducer, setNewProducer] = useState(false);
  const [readOnly, setReadOnly] = useState(true);

  // new stuff
  const handleOnCloseDialogProducer = () => {
    setOpenDialogProducer(false);
    setNewProducer(false);
    setEditProducer(false);
    setRowData({});
  };

  const handleOnClickRow = row => {
    setRowData(row);
    setReadOnly(true);
    setOpenDialogProducer(true);
  };

  const handleOnClickAddProducer = () => {
    setNewProducer(true);
    setReadOnly(false);
    setOpenDialogProducer(true);
  };

  const handleOnSubmitDatosDelProductor = productorData => {
    if (editProducer) {
      putProductores(productorData, {
        onError: () => {
          alert('error');
          handleOnCloseDialogProducer();
        },
        onSuccess: () => {
          alert('El productor ah sido editado con exito');
          handleOnCloseDialogProducer();
        },
      });
    } else {
      postProductores(productorData, {
        onError: () => {
          alert('error');
          handleOnCloseDialogProducer();
        },
        onSuccess: () => {
          alert('El formulario ah sido guardado con exito');
          handleOnCloseDialogProducer();
        },
      });
    }
  };

  if (isSuccess) {
    const provincias = GetLookUp(
      new Set(data.map(element => element.provincia)),
    );
    const distritos = GetLookUp(new Set(data.map(e => e.distrito)));
    const corregimientos = GetLookUp(new Set(data.map(e => e.corregimiento)));
    const lugares = GetLookUp(new Set(data.map(e => e.lugar)));

    const TableActions = [
      {
        icon: 'add',
        tooltip: 'Agregar un nuevo productor',
        isFreeAction: true,
        onClick: event => {
          handleOnClickAddProducer();
        },
      },
      {
        icon: 'delete',
        tooltip: 'borrar este productor',
        onClick: async (event, row) => {
          const res = window.confirm(
            'Realmente desea borrar este productor y sus registros?',
          );
          if (res) {
            // obtener el id del productor.
            const productorID = row._id;

            // obtener lista de registros
            const response = await Axios.get(
              `${Config.API}/registros/productor/${productorID}`,
            );
            const registrosDelProductor = response.data;

            // verificamos is existe registros
            if (registrosDelProductor.length !== 0) {
              // borrar todos los registros del productor
              registrosDelProductor.forEach(registro => {
                deleteRegistros(registro._id);
              });
            }

            deleteProductores(row._id);
            // deleteForm(row._id);
          }
        },
      },
    ];

    return (
      <div>
        <DialogProductor
          open={openDialogProducer}
          close={handleOnCloseDialogProducer}
          data={rowData}
          readOnly={readOnly}
          newData={newProducer}
          setEdit={setEditProducer}
          setReadOnly={setReadOnly}
          handleOnSubmit={handleOnSubmitDatosDelProductor}
        />
        <MaterialTable
          title="Lista de productores."
          columns={[
            {
              title: 'Nombre del Productor',
              field: 'nombreProductor',
            },
            {
              title: 'Cedula',
              field: 'cedula',
            },
            {
              title: 'Provincia',
              field: 'provincia',
              lookup: provincias,
            },
            {
              title: 'Distrito',
              field: 'distrito',
              lookup: distritos,
            },
            {
              title: 'Corregimiento',
              field: 'corregimiento',
              lookup: corregimientos,
            },
            {
              title: 'Lugar',
              field: 'lugar',
              lookup: lugares,
            },
          ]}
          onRowClick={(event, row) => {
            handleOnClickRow(row);
          }}
          options={{
            filtering: true,
            pageSize: 10,
            pageSizeOptions: [10, 20, 30],
          }}
          actions={login ? TableActions : []}
          data={data}
          detailPanel={rowDetail => (
            <DetailPanelRegister producerID={rowDetail._id} />
          )}
        />
      </div>
    );
  }
  if (isLoading) {
    return <div> Loading... </div>;
  }

  return <div />;
};

const Formulario = () => {
  const classes = styles();

  return (
    <div className={classes.root}>
      <Table />
    </div>
  );
};

export default Formulario;
