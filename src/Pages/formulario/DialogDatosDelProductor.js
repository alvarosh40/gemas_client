import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Dialog,
  DialogContent,
  DialogTitle,
  useMediaQuery,
  useTheme,
  Typography,
  Button,
  Toolbar,
} from '@material-ui/core';

import FormDatosDelProductor from '../forms/formDatosDelProductor';

const styles = makeStyles(theme => ({
  root: {},
  action: {
    marginLeft: 'auto',
  },
}));

const DialogDatosDelProductor = props => {
  const {
    open,
    close,
    data,
    readOnly,
    newData,
    setEdit,
    setReadOnly,
    handleOnSubmit,
  } = props;
  const classes = styles();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Dialog
      open={open}
      onClose={close}
      fullScreen={fullScreen}
      maxWidth="lg"
      fullWidth
    >
      <DialogTitle>
        <Toolbar>
          <Typography variant="h5">Datos del productor</Typography>

          <div className={classes.action}>
            {newData ? (
              <div />
            ) : (
              <Button
                variant="contained"
                onClick={() => {
                  setEdit(true);
                  setReadOnly(false);
                }}
              >
                Editar
              </Button>
            )}
          </div>
        </Toolbar>
      </DialogTitle>
      <DialogContent>
        <FormDatosDelProductor
          rowData={data}
          readOnly={readOnly}
          handleOnSubmit={handleOnSubmit}
        />
      </DialogContent>
    </Dialog>
  );
};

export default DialogDatosDelProductor;
