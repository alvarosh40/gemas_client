import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Button } from '@material-ui/core';

const backgroundImage = 'https://placeimg.com/1000/500/nature';
const styles = makeStyles(theme => ({
  root: {},
  content: {
    display: 'flex',
    justifyContent: 'center',
  },
  main: {
    flexGrow: 1,
    maxWidth: 1020,
  },
  heroLayout: {
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    position: 'relative',
    minHeight: '100vh',
    width: '100%',
    backgroundImage: `url(${backgroundImage})`,
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
  },
  layoutOverlay: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  heroLayoutContent: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 0,
  },
  heroLayoutButton: {
    width: 350,
    height: 70,
    borderRadius: 35,
    fontSize: theme.typography.h5.fontSize,
  },
  information: {
    padding: theme.spacing(2),
  },
  informationTitle: {
    display: 'flex',
    justifyContent: 'center',
  },
  informationContent: {},
  footer: {
    height: 100,
    background: 'gray',
    display: 'flex',
    flexDirection: 'column',
  },
}));

const Home = () => {
  const classes = styles();

  return (
    <div className={classes.root}>
      <div className={classes.heroLayout}>
        <div className={classes.layoutOverlay} />
        <div className={classes.heroLayoutContent}>
          <Typography
            variant="h3"
            align="center"
            style={{
              color: 'white',
              fontWeight: 'bold',
            }}
          >
            SISTEMA DE REGISTRO DE PRODUCTORES
          </Typography>
          <Typography
            variant="h5"
            align="center"
            style={{
              color: 'white',
            }}
          >
            Gestion y control de los distintos productores.
          </Typography>
          <div
            style={{
              marginTop: 32,
            }}
          >
            <Button
              className={classes.heroLayoutButton}
              variant="contained"
              color="primary"
              component={Link}
              to="/formulario"
            >
              Buscar
            </Button>
          </div>
        </div>
      </div>
      {/* <div className={classes.content}>
        <div className={classes.main}>
          <div className={classes.information}>
            <div className={classes.informationTitle}>
              <Typography variant="h5">TITLE</Typography>
            </div>
            <div className={classes.informationContent}>
              <Typography>
                El sistema de registro de productores, es un sistema que
                gestiona a los productores.
                <br />
                Guarda y almacena las encuestas dadas a los diversos
                productores.
              </Typography>
            </div>
          </div>
        </div>
      </div> */}
      <div className={classes.footer}>
        <div
          style={{
            flexGrow: 1,
          }}
        />
        <div
          style={{
            alignItems: 'flex-end',
          }}
        >
          <Typography align="center" style={{ color: 'white' }}>
            Gemas
          </Typography>
        </div>
      </div>
    </div>
  );
};

export default Home;
